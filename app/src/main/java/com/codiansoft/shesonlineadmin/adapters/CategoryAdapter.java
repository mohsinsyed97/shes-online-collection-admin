package com.codiansoft.shesonlineadmin.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.codiansoft.shesonlineadmin.R;
import com.codiansoft.shesonlineadmin.models.Category;
import com.codiansoft.shesonlineadmin.models.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by CodianSoft on 20/07/2018.
 */

public class CategoryAdapter extends ArrayAdapter {

    private List<Category> mCategories = new ArrayList<>();
    private Context mContext;

    // Constructor
    public CategoryAdapter(Context context) {
        super(context, R.layout.custom_category_item);
        mContext = context;
    }

    // Overridden methods
    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_category_item, parent, false);

        TextView mTextViewCategoryName = (TextView) view.findViewById(R.id.tv_category_name);
        final ImageView mImageViewEdit = (ImageView) view.findViewById(R.id.img_edit);
        final ImageView mImageViewDel = (ImageView) view.findViewById(R.id.img_delete);

        Category category = mCategories.get(position);
        mTextViewCategoryName.setText(category.getCategoryName());

        mImageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.isNetworkAvailable(mContext)) {
                    Toast.makeText(mContext, "No network available. Please check your internet connection", Toast.LENGTH_LONG).show();
                    return;
                }

                showUpdateCategoryDialog(mCategories.get(position).getCategoryId(),
                            mCategories.get(position).getCategoryName());

                notifyDataSetChanged();
            }
        });

        mImageViewDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.isNetworkAvailable(mContext)) {
                    Toast.makeText(mContext, "No network available. Please check your internet connection", Toast.LENGTH_LONG).show();
                    return;
                }

                showDeleteCategoryDialog(mCategories.get(position).getCategoryId(), position);
                notifyDataSetChanged();
            }
        });

        return view;
    }

    @Nullable
    @Override
    public Category getItem(int position) {
        return mCategories.get(position);
    }

    @Override
    public int getCount() {
        return mCategories.size();
    }

    public void setCategories(List<Category> categories) {
        mCategories = categories;
        notifyDataSetChanged();
    }

    private void showUpdateCategoryDialog(final String categoryId, String categoryName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setCancelable(true);
        builder.setTitle("Edit a category");

        View dialogView = LayoutInflater.from(mContext).inflate(R.layout.custom_category_edit, null, false);
        builder.setView(dialogView);

        final MaterialEditText materialEditTextName = (MaterialEditText) dialogView.findViewById(R.id.et_name);
        Button buttonCancel = (Button) dialogView.findViewById(R.id.btn_cancel);
        Button buttonUpdate = (Button) dialogView.findViewById(R.id.btn_update);

        materialEditTextName.setText(categoryName);
        materialEditTextName.setSelection(materialEditTextName.getText().length());

        final AlertDialog dialog = builder.create();
        dialog.show();

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = materialEditTextName.getText().toString().trim();

                if (name.isEmpty()) {
                    materialEditTextName.setError("Name is required");
                } else if (!Pattern.matches("^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", name)) {
                    materialEditTextName.setError("Invalid name");
                } else {

                    DatabaseReference reference = FirebaseDatabase.getInstance().getReference("category");
                    Category category = new Category(categoryId, name);
                    reference.child(categoryId).setValue(category);

                    Toast.makeText(mContext, "Category updated successfully!", Toast.LENGTH_SHORT).show();

                    dialog.dismiss();
                }
            }
        });
    }


    private void showDeleteCategoryDialog(final String categoryId, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Delete");
        builder.setMessage("Are you sure you wanna delete this category? Once you delete this category, it cannot be undone.");

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                final DatabaseReference dbReferenceCategory = FirebaseDatabase.getInstance().getReference("category").child(categoryId);

                // Check if category has children
                DatabaseReference dbReferenceProduct = FirebaseDatabase.getInstance().getReference("product").child(categoryId);

                dbReferenceProduct.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot.hasChildren()) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setMessage("Cannot delete this category, please delete the products associated with this category." );

                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (dialog != null) {
                                        dialog.dismiss();
                                    }
                                }
                            });

                            AlertDialog dialog = builder.create();
                            dialog.show();

                        } else {
//                            if (mCategories.size() == 1) {
//                                mCategories.clear();
//                            }
//                            else {
//                                mCategories.remove(position);
//                            }
                            dbReferenceCategory.removeValue();
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            Toast.makeText(mContext, "Category deleted successfully!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
