package com.codiansoft.shesonlineadmin.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.codiansoft.shesonlineadmin.R;
import com.codiansoft.shesonlineadmin.activities.AddProductActivity;
import com.codiansoft.shesonlineadmin.fragments.ProductFragment;
import com.codiansoft.shesonlineadmin.models.Product;
import com.codiansoft.shesonlineadmin.models.Utils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CodianSoft on 20/07/2018.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private List<Product> mProducts = new ArrayList<>();
    private Context mContext;
    private OnItemClickListener mItemClickListener;

    // Constructor
    public ProductAdapter(Context context, OnItemClickListener itemClickListener) {
        mContext = context;
        mItemClickListener = itemClickListener;
    }

    // Overridden methods

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Fresco.initialize(parent.getContext());
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_product_item, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, int position) {
        final Product product = mProducts.get(position);

        holder.mSimpleDraweeViewProduct.setImageURI(Uri.parse(product.getProductImages().get(0)));
        holder.mTextViewName.setText(product.getProductName());
        holder.mTextViewDesc.setText(product.getProductDescription());
        holder.mTextViewPrice.setText("Rs. " + product.getProductPrice());

//        holder.mImageViewOptions.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showPopupMenu(holder.mImageViewOptions, product);
//            }
//        });

        holder.mImageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.isNetworkAvailable(mContext)) {
                    Toast.makeText(mContext, "No network available. Please check your internet connection", Toast.LENGTH_LONG).show();
                    return;
                }

                showDeleteDialog(product.getProductId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    // Helper methods

    public Product getItem(int position) {
        return mProducts.get(position);
    }

    public void setProducts(List<Product> products) {
        mProducts = products;
        notifyDataSetChanged();
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view, Product product) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(product));

        MenuPopupHelper menuHelper = new MenuPopupHelper(mContext, (MenuBuilder) popup.getMenu(), view);
        menuHelper.setForceShowIcon(true);
        menuHelper.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        Product mProduct;

        public MyMenuItemClickListener(Product product) {
            mProduct = product;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_edit:

                    if (!Utils.isNetworkAvailable(mContext)) {
                        Toast.makeText(mContext, "No network available. Please check your internet connection", Toast.LENGTH_LONG).show();
                        return false;
                    }

                    Intent intentProduct = new Intent(mContext, AddProductActivity.class);

                    Bundle bundle = new Bundle();
                    bundle.putString(ProductFragment.KEY_PRODUCT_ID, mProduct.getProductId());
                    bundle.putString(ProductFragment.KEY_PRODUCT_NAME, mProduct.getProductName());
                    bundle.putStringArrayList(ProductFragment.KEY_PRODUCT_IMAGES, mProduct.getProductImages());
                    bundle.putString(ProductFragment.KEY_PRODUCT_DESC, mProduct.getProductDescription());
                    bundle.putDouble(ProductFragment.KEY_PRODUCT_PRICE, mProduct.getProductPrice());

                    bundle.putInt(AddProductActivity.KEY_PRODUCT_CATEGORY_INDEX, Utils.sCategoryIndex);

                    intentProduct.putExtras(bundle);
                    intentProduct.putExtra(AddProductActivity.ACTIVITY_TITLE, "Edit a product");

                    mContext.startActivity(intentProduct);
                    return true;

                case R.id.action_delete:

                    if (!Utils.isNetworkAvailable(mContext)) {
                        Toast.makeText(mContext, "No network available. Please check your internet connection", Toast.LENGTH_LONG).show();
                        return false;
                    }

                    showDeleteDialog(mProduct.getProductId());
                    return true;

                default:
            }
            return false;
        }
    }

    // ViewHolder class
    public class ProductViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView mSimpleDraweeViewProduct;
        private TextView mTextViewName, mTextViewDesc, mTextViewPrice;
//        private ImageView mImageViewOptions;
        private ImageView mImageViewDelete;

        public ProductViewHolder(View view) {
            super(view);
            mSimpleDraweeViewProduct = (SimpleDraweeView) view.findViewById(R.id.img_product);
            mTextViewName = (TextView) view.findViewById(R.id.tv_product_name);
            mTextViewDesc = (TextView) view.findViewById(R.id.tv_product_desc);
            mTextViewPrice = (TextView) view.findViewById(R.id.tv_product_price);
//            mImageViewOptions = (ImageView) view.findViewById(R.id.img_options);
            mImageViewDelete = (ImageView) view.findViewById(R.id.img_delete);

            mTextViewDesc.setVisibility(View.GONE);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemClickListener.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    // Item click interface
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    private void showDeleteDialog(final String productId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Delete");
        builder.setMessage("Are you sure you wanna delete this product? Once you delete this product, it cannot be undone.");

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatabaseReference dbReference = FirebaseDatabase.getInstance().getReference("product")
                        .child(Utils.sCategoryId).child(productId);
                dbReference.removeValue();
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
