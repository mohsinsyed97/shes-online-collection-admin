package com.codiansoft.shesonlineadmin.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.codiansoft.shesonlineadmin.R;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CodianSoft on 31/07/2018.
 */

public class ProductImageAdapter extends RecyclerView.Adapter<ProductImageAdapter.ProductImageViewHolder> {

    private List<Uri> mProductImages = new ArrayList<>();
    private Context mContext;

    // Constructor
    public ProductImageAdapter(Context context) {
        mContext = context;
    }

    // Overridden methods

    @Override
    public ProductImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Fresco.initialize(parent.getContext());
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_product_image_item, parent, false);
        return new ProductImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductImageViewHolder holder, final int position) {
        final Uri productImage = mProductImages.get(position);

        holder.mSimpleDraweeViewProduct.setImageURI(productImage);
        holder.mImageViewClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProductImages.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, mProductImages.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProductImages.size();
    }

    // Helper methods

    public Uri getItem(int position) {
        return mProductImages.get(position);
    }

    public void setProductImages(List<Uri> productImages) {
        mProductImages = productImages;
        notifyDataSetChanged();
    }

    // ViewHolder class
    public class ProductImageViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView mSimpleDraweeViewProduct;
        private ImageView mImageViewClear;

        public ProductImageViewHolder(View view) {
            super(view);
            mSimpleDraweeViewProduct = (SimpleDraweeView) view.findViewById(R.id.img_product);
            mImageViewClear = (ImageView) view.findViewById(R.id.img_clear);
        }
    }

    // Item click interface
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
