package com.codiansoft.shesonlineadmin.models;

/**
 * Created by CodianSoft on 31/07/2018.
 */

public class User {

    private String mEmail, mPass;

    public User() {
    }

    public User(String email, String pass) {
        mEmail = email;
        mPass = pass;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPass() {
        return mPass;
    }

    public void setPass(String pass) {
        mPass = pass;
    }
}
