package com.codiansoft.shesonlineadmin.models;

/**
 * Created by CodianSoft on 27/07/2018.
 */

public class Category {

    private String mCategoryId, mCategoryName;

    public Category() {
    }

    public Category(String categoryId, String categoryName) {
        mCategoryId = categoryId;
        mCategoryName = categoryName;
    }

    public String getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(String categoryId) {
        mCategoryId = categoryId;
    }

    public String getCategoryName() {
        return mCategoryName;
    }

    public void setCategoryName(String categoryName) {
        mCategoryName = categoryName;
    }

    @Override
    public String toString() {
        return mCategoryName;
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;
        if (obj != null && obj instanceof Category) {
            isEqual = (this.mCategoryName == ((Category) obj).mCategoryName);
        }
        return isEqual;
    }
}
