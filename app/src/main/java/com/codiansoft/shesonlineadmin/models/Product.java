package com.codiansoft.shesonlineadmin.models;

import java.util.ArrayList;

/**
 * Created by CodianSoft on 27/07/2018.
 */

public class Product {

    private String productId, productName;
    private ArrayList<String> mProductImages = new ArrayList<>();
    private Double productPrice;
    private String productDescription;

    private static ArrayList<Product> sProductsCartList = new ArrayList<>();

    public Product() {
    }

    public Product(String productId, String productName, ArrayList<String> productImages, Double productPrice, String productDescription) {
        this.productId = productId;
        this.productName = productName;
        this.mProductImages = productImages;
        this.productPrice = productPrice;
        this.productDescription = productDescription;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public ArrayList<String> getProductImages() {
        return mProductImages;
    }

    public void setProductImages(ArrayList<String> productImages) {
        mProductImages = productImages;
    }

    public Double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Double productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public static ArrayList<Product> getItemsFromProductsCartList() {
        return sProductsCartList;
    }

    public static void addItemsToProductsCartList(Product product) {
        sProductsCartList.add(0, product);
    }

    public void removeItemFromProductCartList(int position) {
        this.sProductsCartList.remove(position);
    }
}
