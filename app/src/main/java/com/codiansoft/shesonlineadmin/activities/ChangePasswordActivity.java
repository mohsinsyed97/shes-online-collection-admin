package com.codiansoft.shesonlineadmin.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.codiansoft.shesonlineadmin.R;
import com.codiansoft.shesonlineadmin.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;


public class ChangePasswordActivity extends AppCompatActivity {

    private MaterialEditText mMaterialEditTextOldPass, mMaterialEditTextNewPass, mMaterialEditTextConfirmPass;
    private Button mButtonUpdate;

    private DatabaseReference mDatabaseReferenceUser;
    private String mAdminEmail = "", mAdminPass = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mMaterialEditTextOldPass = (MaterialEditText) findViewById(R.id.et_old_pass);
        mMaterialEditTextNewPass = (MaterialEditText) findViewById(R.id.et_new_pass);
        mMaterialEditTextConfirmPass = (MaterialEditText) findViewById(R.id.et_confirm_pass);
        mButtonUpdate = (Button) findViewById(R.id.btn_update);

        mDatabaseReferenceUser = FirebaseDatabase.getInstance().getReference("user").child("admin_1");

        mButtonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    User user = new User(mAdminEmail, mMaterialEditTextConfirmPass.getText().toString().trim());
                    mDatabaseReferenceUser.setValue(user);
                    Toast.makeText(ChangePasswordActivity.this, "Password updated successfully!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        mDatabaseReferenceUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                mAdminEmail = user.getEmail();
                mAdminPass = user.getPass();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private boolean isValid() {
        boolean check = false;

        String oldPass = mMaterialEditTextOldPass.getText().toString().trim();
        String newPass = mMaterialEditTextNewPass.getText().toString().trim();
        String confirmPass = mMaterialEditTextConfirmPass.getText().toString().trim();

        if (oldPass.isEmpty()) {
            showError(mMaterialEditTextOldPass, "This field is required");
        }
        else if (!oldPass.equals(mAdminPass)) {
            showError(mMaterialEditTextOldPass, "Incorrect password");
        }
        else if (newPass.isEmpty()) {
            showError(mMaterialEditTextNewPass, "This field is required");
        }
        else if (confirmPass.isEmpty()) {
            showError(mMaterialEditTextConfirmPass, "This field is required");
        }
        else if (!newPass.equals(confirmPass)) {
            showError(mMaterialEditTextConfirmPass, "Password does not match");
        }
        else {
            check = true;
        }
        return check;
    }

    private void showError(MaterialEditText materialEditText, String errMsg) {
        materialEditText.setError(errMsg);
        materialEditText.requestFocus();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }    }
}
