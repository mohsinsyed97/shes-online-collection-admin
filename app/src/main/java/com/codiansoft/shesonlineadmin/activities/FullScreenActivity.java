package com.codiansoft.shesonlineadmin.activities;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import com.codiansoft.shesonlineadmin.R;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

public class FullScreenActivity extends AppCompatActivity {

    private SimpleDraweeView mSimpleDraweeViewProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(getApplicationContext());
        setContentView(R.layout.activity_full_screen);

        getSupportActionBar().hide();

        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            // Remember that you should never show the action bar if the
            // status bar is hidden, so hide that too if necessary.
        }

        mSimpleDraweeViewProduct = (SimpleDraweeView) findViewById(R.id.img_product);
        mSimpleDraweeViewProduct.setImageURI(Uri.parse(getIntent()
                .getStringExtra(ProductDetailsActivity.KEY_PRODUCT_CURRENT_SLIDER_IMAGE)));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
