package com.codiansoft.shesonlineadmin.activities;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.codiansoft.shesonlineadmin.R;
import com.codiansoft.shesonlineadmin.adapters.ProductImageAdapter;
import com.codiansoft.shesonlineadmin.fragments.ProductFragment;
import com.codiansoft.shesonlineadmin.models.Category;
import com.codiansoft.shesonlineadmin.models.Product;
import com.codiansoft.shesonlineadmin.models.Utils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AddProductActivity extends AppCompatActivity {

    private EditText mEditTextName, mEditTextPrice, mEditTextDesc;
    private RecyclerView mRecyclerViewProductImages;
    private Spinner mSpinnerCategory;
    private Button mButtonBrowse;

    private ProgressDialog mProgressDialog;

    private List<Category> mCategories;

    private List<Uri> mProductImages;
    private ProductImageAdapter mProductImageAdapter;

    private static final String LOG_TAG = AddProductActivity.class.getSimpleName();
    private static final String STORAGE_PATH_UPLOADS = "uploads/";

    private static final int REQUEST_GALLERY = 99;

    private DatabaseReference mDbReferenceCategory, mDbReferenceProduct;
    private StorageReference mStorageReference;

    private String mCategoryId, mProductIdEditMode = "";

    String mImageEncoded;
    List<String> mImagesEncodedList;

    public static final String ACTIVITY_TITLE = "activity-title";
    public static final String KEY_PRODUCT_CATEGORY_INDEX = "product-category-index";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getIntent().getStringExtra(ACTIVITY_TITLE));

        mRecyclerViewProductImages = (RecyclerView) findViewById(R.id.rv_product_images);
        mEditTextName = (EditText) findViewById(R.id.et_name);
        mEditTextPrice = (EditText) findViewById(R.id.et_price);
        mEditTextDesc = (EditText) findViewById(R.id.et_desc);
        mButtonBrowse = (Button) findViewById(R.id.btn_browse);
        mSpinnerCategory = (Spinner) findViewById(R.id.sp_category);

        mEditTextDesc.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.et_desc) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        mCategories = new ArrayList<Category>();
        mDbReferenceCategory = FirebaseDatabase.getInstance().getReference("category");

        mStorageReference = FirebaseStorage.getInstance().getReference();

        mButtonBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                galleryIntent();
            }
        });

        mSpinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                mCategoryId = mCategories.get(pos).getCategoryId();
                mDbReferenceProduct = FirebaseDatabase.getInstance().getReference("product").child(mCategoryId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // Set up values for edit mode
        Bundle bundle = getIntent().getExtras();
        if (getSupportActionBar().getTitle().equals("Edit a product")) {
            // Id
            mProductIdEditMode = bundle.getString(ProductFragment.KEY_PRODUCT_ID);

            // Images
            mProductImages = new ArrayList<>();
            List<String> images = bundle.getStringArrayList(ProductFragment.KEY_PRODUCT_IMAGES);
            for (int i = 0; i < images.size(); i++) {
                mProductImages.add(Uri.parse(images.get(i)));
            }
            mProductImageAdapter = new ProductImageAdapter(getApplicationContext());
            mRecyclerViewProductImages.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
            mRecyclerViewProductImages.setAdapter(mProductImageAdapter);
            mProductImageAdapter.setProductImages(mProductImages);

            // Name
            mEditTextName.setText(bundle.getString(ProductFragment.KEY_PRODUCT_NAME));

            // Price
            mEditTextPrice.setText(String.valueOf(bundle.getDouble(ProductFragment.KEY_PRODUCT_PRICE)));

            // Desc
            mEditTextDesc.setText(bundle.getString(ProductFragment.KEY_PRODUCT_DESC));
        } else {
            mProductImages = new ArrayList<>();
            mProductImageAdapter = new ProductImageAdapter(getApplicationContext());
            mRecyclerViewProductImages.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
            mRecyclerViewProductImages.setAdapter(mProductImageAdapter);
        }

        for (Uri productImage : mProductImages) {
            Log.v("Image --->", productImage.toString());
        }

        mProgressDialog = new ProgressDialog(AddProductActivity.this);
        mProgressDialog.setTitle("Loading");
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);

        showProgressDialog();

        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            mProgressDialog.dismiss();
            Toast.makeText(getApplicationContext(), "No network available. Please check your internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        mDbReferenceCategory.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                // Clear all previous data
                mCategories.clear();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Category category = snapshot.getValue(Category.class);
                    mCategories.add(category);
                }

                dismissProgressDialog();

                if (!mCategories.isEmpty()) {
                    Collections.reverse(mCategories);
                    ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(AddProductActivity.this,
                            android.R.layout.simple_spinner_item,
                            mCategories);
                    mSpinnerCategory.setAdapter(adapter);
                    mSpinnerCategory.setSelection(Utils.sCategoryIndex);

                    // Set up category spinner value for edit mode
                    Bundle bundle = getIntent().getExtras();
                    if (getSupportActionBar().getTitle().equals("Edit a product")) {
                        mSpinnerCategory.setSelection(bundle.getInt(AddProductActivity.KEY_PRODUCT_CATEGORY_INDEX));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                dismissProgressDialog();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_product, menu);
        MenuItem menuItem = menu.findItem(R.id.action_add);
        if (getSupportActionBar().getTitle().equals("Add new product")) {
            menuItem.setTitle("Add");
        } else {
            menuItem.setTitle("Update");
        }
        return true;
    }

    private void showProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.show();
        }
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
                finish();
                return true;

            case R.id.action_add:
                if (getSupportActionBar().getTitle().equals("Edit a product")) {
                    editProduct();
                } else {
                    addProduct();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addProduct() {

        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            dismissProgressDialog();
            Toast.makeText(getApplicationContext(), "No network available. Please check your internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        if (isValid()) {

            final String id = mDbReferenceProduct.push().getKey();
            final String name = mEditTextName.getText().toString().trim();
            final String priceStr = mEditTextPrice.getText().toString().trim();
            final String desc = mEditTextDesc.getText().toString().trim();

            final ArrayList<String> imageUrlsList = new ArrayList<String>();

            for (Uri productImage : mProductImages) {

                // getting the storage reference
                StorageReference sRef = mStorageReference.child(STORAGE_PATH_UPLOADS + "/" + id + "/"
                        + System.currentTimeMillis() + "." + getFileExtension(productImage));

                // adding the file to reference
                UploadTask uploadTask = sRef.putFile(productImage);

                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        String imageUrl = taskSnapshot.getDownloadUrl().toString();
                        imageUrlsList.add(imageUrl);

                        Product product = new Product(id, name, imageUrlsList, Double.parseDouble(priceStr), desc);

                        // adding to firebase database
                        mDbReferenceProduct.child(id).setValue(product);
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                Log.e(LOG_TAG + ", Exception: ", exception.toString());
                                Toast.makeText(AddProductActivity.this, "Something went wrong. Please try again", Toast.LENGTH_LONG)
                                        .show();
                            }
                        })
                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                // displaying the upload progress
//                                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
//                                mProgressDialog.setMessage("Adding a product\n(" + ((int) progress) + "%).");
                            }
                        });
            }

            // displaying success toast
            Utils.sCategoryIndex = mSpinnerCategory.getSelectedItemPosition();
            String resultStr = "Product added successfully!";
            Toast.makeText(AddProductActivity.this, resultStr, Toast.LENGTH_SHORT).show();
            Log.v("Product ->", resultStr);
            finish();

        }
    }

    private void editProduct() {

        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            dismissProgressDialog();
            Toast.makeText(getApplicationContext(), "No network available. Please check your internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        if (isValid()) {

            final DatabaseReference dbReference = FirebaseDatabase.getInstance().getReference("product").child(mCategoryId);

            final String id = mProductIdEditMode;
            final String name = mEditTextName.getText().toString().trim();
            final String priceStr = mEditTextPrice.getText().toString().trim();
            final String desc = mEditTextDesc.getText().toString().trim();

            final ArrayList<String> imageUrlsList = new ArrayList<String>();

            for (Uri productImage : mProductImages) {

                // getting the storage reference
                StorageReference sRef = mStorageReference.child(STORAGE_PATH_UPLOADS + "/" + id + "/"
                        + System.currentTimeMillis() + "." + getFileExtension(productImage));

                // adding the file to reference
                UploadTask uploadTask = sRef.putFile(productImage);

                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        String imageUrl = taskSnapshot.getDownloadUrl().toString();
                        imageUrlsList.add(imageUrl);

                        Product product = new Product(id, name, imageUrlsList, Double.parseDouble(priceStr), desc);

                        // adding to firebase database
                        dbReference.child(id).setValue(product);
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                Log.e(LOG_TAG + ", Exception: ", exception.toString());
                                Toast.makeText(AddProductActivity.this, "Something went wrong. Please try again", Toast.LENGTH_LONG)
                                        .show();
                            }
                        })
                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                // displaying the upload progress
//                                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
//                                mProgressDialog.setMessage("Updating a product\n(" + ((int) progress) + "%).");
                            }
                        });
            }

            // displaying success toast
            String resultStr = "Product updated successfully!";
            Toast.makeText(AddProductActivity.this, resultStr, Toast.LENGTH_SHORT).show();
            Utils.sCategoryId = id;
            Log.v("Product ->", resultStr);
            finish();
        }
    }

    // Helper method to validate product data
    private boolean isValid() {
        boolean check = false;

        String name = mEditTextName.getText().toString().trim();
        String priceStr = mEditTextPrice.getText().toString().trim();
        String desc = mEditTextDesc.getText().toString().trim();

        if (mProductImages.size() == 0 || name.isEmpty() || priceStr.isEmpty() || desc.isEmpty()) {
            Toast.makeText(this, "All fields are required.", Toast.LENGTH_SHORT).show();
        } else {
            check = true;
        }
        return check;
    }

    // Helper method to open gallery
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Images(s)"), REQUEST_GALLERY);
    }

    public String getFileExtension(Uri uri) {
        ContentResolver resolver = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(resolver.getType(uri));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            // When an Image is picked
            if (requestCode == REQUEST_GALLERY && resultCode == RESULT_OK) {

                if (data != null) {

                    // For more than one images
                    if (data.getClipData() != null) {

                        //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
                        int count = data.getClipData().getItemCount();

                        for (int i = 0; i < count; i++) {
                            Uri imageUri = data.getClipData().getItemAt(i).getUri();
                            mProductImages.add(imageUri);
                        }
                        mProductImageAdapter.setProductImages(mProductImages);
                    }

//                    // For more one image
                    else {
                        // do something with the image (save it to some directory or whatever you need to do with it here)
                        String imagePath = data.getData().getPath();

                        Uri uri = data.getData();
                        mProductImages.add(uri);
                        mProductImageAdapter.setProductImages(mProductImages);
                    }
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong. Please try again", Toast.LENGTH_LONG)
                    .show();
        }

        for (Uri productImage : mProductImages) {
            Log.v("Image --->", productImage.toString());
        }
    }
}
