package com.codiansoft.shesonlineadmin.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.codiansoft.shesonlineadmin.R;
import com.codiansoft.shesonlineadmin.fragments.CategoriesFragment;
import com.codiansoft.shesonlineadmin.fragments.ProductFragment;
import com.codiansoft.shesonlineadmin.models.Category;
import com.codiansoft.shesonlineadmin.models.Utils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.regex.Pattern;

import static com.codiansoft.shesonlineadmin.fragments.ProductFragment.sCategories;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private FloatingActionButton mFloatingActionButton;
    private Toolbar mToolbar;

    // index to identify current nav menu item
    public static int sNavItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_PRODUCTS = "products";
    private static final String TAG_CATEGORIES = "categories";
    public static String CURRENT_TAG = TAG_PRODUCTS;

    // mToolbar titles respected to selected nav menu item
    private String[] mActivityTitles;

    // flag to load home fragment when user presses back key
    private boolean mShouldLoadHomeFragOnBackPress = true;

    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);


        mHandler = new Handler();

        mFloatingActionButton = (FloatingActionButton) findViewById(R.id.fab_add);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.isNetworkAvailable(getApplicationContext())) {
                    Toast.makeText(getApplicationContext(), "No network available. Please check your internet connection", Toast.LENGTH_LONG).show();
                    return;
                }

                if (sNavItemIndex == 0) {
                    if (sCategories.size() > 0) {
                        Intent intentProduct = new Intent(MainActivity.this, AddProductActivity.class);
                        intentProduct.putExtra(AddProductActivity.ACTIVITY_TITLE, "Add new product");
                        startActivity(intentProduct);
                    }
                    else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("To add new product you need to add at least one category." );

                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }

                if (sNavItemIndex == 1) {
                    showAddCategoryDialog();
                }
            }
        });

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        // load mToolbar titles from string resources
        mActivityTitles = new String[]{"Products", "Categories"};

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            sNavItemIndex = 0;
            CURRENT_TAG = TAG_PRODUCTS;
            loadFragmentOnIndexBased();
        }
    }

    private void showAddCategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Add new category");

        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_category_add, null, false);
        builder.setView(dialogView);

        final MaterialEditText materialEditTextName = (MaterialEditText) dialogView.findViewById(R.id.et_name);
        Button buttonCancel = (Button) dialogView.findViewById(R.id.btn_cancel);
        Button buttonAdd = (Button) dialogView.findViewById(R.id.btn_add);

        final AlertDialog dialog = builder.create();
        dialog.show();

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = materialEditTextName.getText().toString().trim();

                if (name.isEmpty()) {
                    materialEditTextName.setError("Name is required");
                } else if (!Pattern.matches("^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", name)) {
                    materialEditTextName.setError("Invalid name");
                } else {

                    DatabaseReference reference = FirebaseDatabase.getInstance().getReference("category");
                    String id = reference.push().getKey();
                    Category category = new Category(id, name);
                    reference.child(id).setValue(category);

                    Toast.makeText(MainActivity.this, "Category added successfully!", Toast.LENGTH_SHORT).show();

                    dialog.dismiss();
                }
            }
        });
    }


    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadFragmentOnIndexBased() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set mToolbar title
        // setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation mDrawerLayout
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            mDrawerLayout.closeDrawers();
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getFragmentOnIndexBased();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        //Closing mDrawerLayout on item click
        mDrawerLayout.closeDrawers();

        // refresh mToolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getFragmentOnIndexBased() {
        switch (sNavItemIndex) {
            case 0:
                // home
                ProductFragment productFragment = new ProductFragment();
                return productFragment;
            case 1:
                // photos
                CategoriesFragment categoriesFragment = new CategoriesFragment();
                return categoriesFragment;

            default:
                return new ProductFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(mActivityTitles[sNavItemIndex]);
    }

    private void selectNavMenu() {
        mNavigationView.getMenu().getItem(sNavItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_products:
                        sNavItemIndex = 0;
                        CURRENT_TAG = TAG_PRODUCTS;
                        break;
                    case R.id.nav_categories:
                        sNavItemIndex = 1;
                        CURRENT_TAG = TAG_CATEGORIES;
                        break;
                    case R.id.nav_change_pass:
                        // launch new intent instead of loading fragment
                        startActivity(new Intent(MainActivity.this, ChangePasswordActivity.class));
                        mDrawerLayout.closeDrawers();
                        return true;
                    default:
                        sNavItemIndex = 0;
                }

                // Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadFragmentOnIndexBased();

                return true;
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the mDrawerLayout closes
                super.onDrawerClosed(drawerView);

                try {
                    //int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        // Do something for lollipop and above versions

                        Window window = getWindow();

                        // clear FLAG_TRANSLUCENT_STATUS flag:
                        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

                        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
                        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

                        // finally change the color to any color with transparency
                        window.setStatusBarColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark));
                    }

                } catch (Exception e) {
                    Log.e(LOG_TAG, e.getMessage());
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the mDrawerLayout open
                super.onDrawerOpened(drawerView);

                try {
                    //int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        // Do something for lollipop and above versions

                        Window window = getWindow();

                        // clear FLAG_TRANSLUCENT_STATUS flag:
                        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

                        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
                        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

                        // finally change the color to any color with transparency
                        window.setStatusBarColor(Color.TRANSPARENT);
                    }

                } catch (Exception e) {
                    Log.e(LOG_TAG, e.getMessage());
                }
            }
        };

        // Setting the actionbarToggle to mDrawerLayout layout
        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);

        // Calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (mShouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (sNavItemIndex != 0) {
                sNavItemIndex = 0;
                CURRENT_TAG = TAG_PRODUCTS;
                loadFragmentOnIndexBased();
                return;
            }
        }

        super.onBackPressed();
    }
}
