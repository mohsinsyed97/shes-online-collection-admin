package com.codiansoft.shesonlineadmin.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.codiansoft.shesonlineadmin.R;
import com.codiansoft.shesonlineadmin.fragments.ProductFragment;
import com.codiansoft.shesonlineadmin.models.Product;
import com.codiansoft.shesonlineadmin.models.Utils;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.facebook.drawee.backends.pipeline.Fresco;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ProductDetailsActivity extends AppCompatActivity {

    private SliderLayout mSliderLayout;
    private TextView mTextViewName, mTextViewDesc, mTextViewPrice;

    Product mProduct;

    public static final String KEY_PRODUCT_CURRENT_SLIDER_IMAGE = "current-slider-image";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(getApplicationContext());
        setContentView(R.layout.activity_product_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSliderLayout = (SliderLayout) findViewById(R.id.slider);
        mTextViewName = (TextView) findViewById(R.id.tv_product_name);
        mTextViewDesc = (TextView) findViewById(R.id.tv_product_desc);
        mTextViewPrice = (TextView) findViewById(R.id.tv_product_price);

        final Bundle bundle = getIntent().getExtras();

        getSupportActionBar().setTitle(bundle.getString(ProductFragment.KEY_PRODUCT_NAME));

        loadProductImagesIntoSlider(bundle);

        mTextViewName.setText(bundle.getString(ProductFragment.KEY_PRODUCT_NAME));
        mTextViewDesc.setText(bundle.getString(ProductFragment.KEY_PRODUCT_DESC));
        mTextViewPrice.setText("Rs. " + bundle.getDouble(ProductFragment.KEY_PRODUCT_PRICE));

        mProduct = new Product(bundle.getString(ProductFragment.KEY_PRODUCT_ID),
                bundle.getString(ProductFragment.KEY_PRODUCT_NAME),
                bundle.getStringArrayList(ProductFragment.KEY_PRODUCT_IMAGES),
                bundle.getDouble(ProductFragment.KEY_PRODUCT_PRICE),
                bundle.getString(ProductFragment.KEY_PRODUCT_DESC)
        );
    }

    private void loadProductImagesIntoSlider(Bundle bundle) {
        HashMap<String, String> images = new HashMap<String, String>();

        int index = 0;
        for (String image : bundle.getStringArrayList(ProductFragment.KEY_PRODUCT_IMAGES)) {
            images.put(bundle.getString(ProductFragment.KEY_PRODUCT_NAME) + "_" + index++, image);
        }

        for (String key : images.keySet()) {

            TextSliderView textSliderView = new TextSliderView(this);

            // initialize a SliderLayout
            textSliderView
//                    .description(key)
                    .image(images.get(key))
                    .setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            Intent intent = new Intent(ProductDetailsActivity.this, FullScreenActivity.class);
                            intent.putExtra(KEY_PRODUCT_CURRENT_SLIDER_IMAGE, slider.getUrl());
                            startActivity(intent);
                        }
                    });


            mSliderLayout.addSlider(textSliderView);
//            mSliderLayout.startAutoCycle();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
