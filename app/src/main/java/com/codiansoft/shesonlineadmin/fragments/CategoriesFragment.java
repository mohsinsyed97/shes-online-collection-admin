package com.codiansoft.shesonlineadmin.fragments;


import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.codiansoft.shesonlineadmin.R;
import com.codiansoft.shesonlineadmin.adapters.CategoryAdapter;
import com.codiansoft.shesonlineadmin.models.Category;
import com.codiansoft.shesonlineadmin.models.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesFragment extends Fragment {

    private ListView mListViewCategories;

    private ProgressDialog mProgressDialog;

    private List<Category> mCategories;

    private CategoryAdapter mCategoryAdapter;

    private DatabaseReference mDbReferenceCategory;

    public CategoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_categories, container, false);

        mDbReferenceCategory = FirebaseDatabase.getInstance().getReference("category");
        mCategories = new ArrayList<Category>();

        mListViewCategories = (ListView) view.findViewById(R.id.lv_categories);
        mCategoryAdapter = new CategoryAdapter(getContext());
        mListViewCategories.setAdapter(mCategoryAdapter);

        mListViewCategories.setOverscrollFooter(new ColorDrawable(Color.TRANSPARENT));

        mListViewCategories.setEmptyView((TextView) view.findViewById(R.id.tv_empty_view));

        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setTitle("Loading");
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        mProgressDialog.show();

        if (!Utils.isNetworkAvailable(getContext())) {
            mProgressDialog.dismiss();
            Toast.makeText(getContext(), "No network available. Please check your internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        mDbReferenceCategory.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Clear all previous data
                mCategories.clear();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Category category = snapshot.getValue(Category.class);
                    mCategories.add(category);
                }

                mProgressDialog.dismiss();

                if (!mCategories.isEmpty()) {
                    Collections.reverse(mCategories);
                    mCategoryAdapter.setCategories(mCategories);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mProgressDialog.dismiss();
            }
        });
    }
}
