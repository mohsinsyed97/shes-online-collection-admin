package com.codiansoft.shesonlineadmin.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.codiansoft.shesonlineadmin.R;
import com.codiansoft.shesonlineadmin.activities.ProductDetailsActivity;
import com.codiansoft.shesonlineadmin.adapters.ProductAdapter;
import com.codiansoft.shesonlineadmin.models.Category;
import com.codiansoft.shesonlineadmin.models.Product;
import com.codiansoft.shesonlineadmin.models.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductFragment extends Fragment {

    private Spinner mSpinnerCategory;
    private RecyclerView mRecyclerViewProducts;
    private TextView mTextViewEmptyView;

    private ProgressDialog mProgressDialog;

    public static List<Category> sCategories;

    private ProductAdapter mProductAdapter;

    private LinearLayout mLinearLayoutSpinner;

    private DatabaseReference mDbReferenceCategory, mDbReferenceProduct;

    private static final int NUM_OF_COLS = 2;

    public static final String KEY_PRODUCT_ID = "product-id";
    public static final String KEY_PRODUCT_NAME = "product-name";
    public static final String KEY_PRODUCT_IMAGES = "product-image";
    public static final String KEY_PRODUCT_DESC = "product-description";
    public static final String KEY_PRODUCT_PRICE = "product-price";

    public ProductFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product, container, false);

        mDbReferenceCategory = FirebaseDatabase.getInstance().getReference("category");
        sCategories = new ArrayList<Category>();

        mLinearLayoutSpinner = (LinearLayout) view.findViewById(R.id.linear_layout_category_spinner);

        mTextViewEmptyView = (TextView) view.findViewById(R.id.tv_empty_view);
        mRecyclerViewProducts = (RecyclerView) view.findViewById(R.id.rv_products);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(NUM_OF_COLS, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerViewProducts.setLayoutManager(layoutManager);

        mProductAdapter = new ProductAdapter(getContext(), new ProductAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Product product = mProductAdapter.getItem(position);

                String toastStr = "Product details!\n\nId: " + product.getProductId()
                        + "\nName: " + product.getProductName()
                        + "\nDescription: " + product.getProductDescription()
                        + "\nPrice: Rs. " + product.getProductPrice();

                // Toast.makeText(getApplicationContext(), toastStr, Toast.LENGTH_LONG).show();

                Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString(KEY_PRODUCT_ID, product.getProductId());
                bundle.putString(KEY_PRODUCT_NAME, product.getProductName());
                bundle.putDouble(KEY_PRODUCT_PRICE, product.getProductPrice());
                bundle.putStringArrayList(KEY_PRODUCT_IMAGES, product.getProductImages());
                bundle.putString(KEY_PRODUCT_DESC, product.getProductDescription());

                intent.putExtras(bundle);

                startActivity(intent);
            }
        });

        mRecyclerViewProducts.setAdapter(mProductAdapter);

        mSpinnerCategory = (Spinner) view.findViewById(R.id.sp_category);
        mSpinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                String categoryId = sCategories.get(pos).getCategoryId();

                // These values stores the selected category index from spinner
                // and pass it to the edit product activity
                // from ProductAdapter
                Utils.sCategoryIndex = pos;

                Utils.sCategoryId = categoryId;

                mProgressDialog.show();

                if (!Utils.isNetworkAvailable(getContext())) {
                    mProgressDialog.dismiss();
                    Toast.makeText(getContext(), "No network available. Please check your internet connection", Toast.LENGTH_LONG).show();
                    return;
                }

                mDbReferenceProduct = FirebaseDatabase.getInstance().getReference("product").child(categoryId);
                mDbReferenceProduct.addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Product> products = new ArrayList<>();

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Product p = snapshot.getValue(Product.class);
                            products.add(new Product(p.getProductId(), p.getProductName(), p.getProductImages(), p.getProductPrice(), p.getProductDescription()));
                        }

                        mProgressDialog.dismiss();

                        if (products.size() > 0) {
                            Collections.reverse(products);
                            mRecyclerViewProducts.setVisibility(View.VISIBLE);
                            mTextViewEmptyView.setVisibility(View.GONE);
                            mProductAdapter.setProducts(products);

                            for (int i = 0; i < products.size(); i++) {
                                Log.v("Products!! -> ",
                                        products.get(i).getProductId() + ", "
                                                + products.get(i).getProductName() + ", "
                                                + products.get(i).getProductDescription() + "\n\n");
                            }
                        }
                        else {
                            mTextViewEmptyView.setVisibility(View.VISIBLE);
                            mRecyclerViewProducts.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        mProgressDialog.dismiss();
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setTitle("Loading");
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        mProgressDialog.show();

        if (!Utils.isNetworkAvailable(getContext())) {
            mProgressDialog.dismiss();
            Toast.makeText(getContext(), "No network available. Please check your internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        mDbReferenceCategory.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {

                    // Clear all previous data
                    sCategories.clear();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Category category = snapshot.getValue(Category.class);
                        sCategories.add(category);
                    }

                    mProgressDialog.dismiss();

                    if (!sCategories.isEmpty()) {
                        Collections.reverse(sCategories);
                        mLinearLayoutSpinner.setVisibility(View.VISIBLE);
                        ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(getContext(),
                                R.layout.custom_spinner_item,
                                sCategories);
                        mSpinnerCategory.setAdapter(adapter);

                        mSpinnerCategory.setSelection(Utils.sCategoryIndex);

                        for (int i = 0; i < sCategories.size(); i++) {
                            Log.v("Category -> ",
                                    sCategories.get(i).getCategoryId() + ","
                                            + sCategories.get(i).getCategoryName() + "\n\n");
                        }
                    }
                    else {
                        mLinearLayoutSpinner.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mProgressDialog.dismiss();
            }
        });
    }
}
